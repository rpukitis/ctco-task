const merge = require('webpack-merge')
const common = require('./common.js')
const CopyPlugin = require('copy-webpack-plugin')

process.env.NODE_ENV = 'production'

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [new CopyPlugin([{ from: '_redirects' }])],
})
