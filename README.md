# C.T.Co TASK

## What it is?

This is C.T.Co home task

## How to use

Install it and run in development mode:

```bash
yarn
yarn start
```

Install it and build in production mode:

```bash
yarn
yarn build
```

Install it and build in analyze mode:

```bash
yarn
yarn analyze
```

Run integration tests:

```bash
yarn test
```

Format code:

```bash
yarn format
```