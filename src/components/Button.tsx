import React from 'react'
import styled from 'styled-components'

const RippleContainer = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  span {
    position: absolute;
    background-color: #1336bc;
    border-radius: 100%;
    opacity: 0.75;
    transform: scale(0);
    animation-name: ripple;
    animation-duration: 850ms;
  }

  @keyframes ripple {
    to {
      opacity: 0;
      transform: scale(2);
    }
  }
`

type REP = {
  x: number
  y: number
  width: number
  height: number
}

function Ripple(): JSX.Element {
  const timeout = React.useRef<number>()
  const [ripples, setRipples] = React.useState<REP[]>([])

  React.useEffect(() => {
    if (ripples.length) {
      timeout.current && clearTimeout(timeout.current)
      timeout.current = setTimeout(() => {
        setRipples([])
      }, 1000)
    }
  }, [ripples])

  function addRipple(e: React.MouseEvent<HTMLDivElement>) {
    const rect = e.currentTarget.getBoundingClientRect()
    const size = rect.width > rect.height ? rect.width : rect.height
    const x = e.pageX - rect.x - size / 2
    const y = e.pageY - rect.y - size / 2
    setRipples([...ripples, { x, y, width: size, height: size }])
  }

  return (
    <RippleContainer onMouseDown={addRipple}>
      {ripples.map((ripple, i) => (
        <span
          key={i}
          style={{
            top: ripple.y,
            left: ripple.x,
            width: ripple.width,
            height: ripple.height,
          }}
        />
      ))}
    </RippleContainer>
  )
}

const ButtonElement = styled.button`
  position: relative;
  padding: 8px 18px;
  font-weight: 700;
  font-size: 21px;
  color: #1336bc;
  overflow: hidden;
  cursor: pointer;
  user-select: none;
`

type Props = {
  children: React.ReactNode
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void
}

export function Button({ children, onClick }: Props) {
  return (
    <ButtonElement type="button" onClick={onClick}>
      {children}
      <Ripple />
    </ButtonElement>
  )
}
