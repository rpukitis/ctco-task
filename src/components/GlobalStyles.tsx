import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  html {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    box-sizing: border-box;
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  body {
    margin: 0;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    font-weight: 400;
    font-size: 0.875rem;
    line-height: normal;
    letter-spacing: normal;
    color: #151413;
    background-color: #F1F3F2;
  }

  button {
    appearance: none;
    -webkit-appearance: none;
    border: none;
    padding: 0;
    outline: none;
    background-color: transparent;
  }
`
