import React from 'react'
import { render } from '@testing-library/react'
import 'jest-styled-components'
import { ProfileListItem } from '../ProfileListItem'

describe('ProfileListItem', () => {
  it('should render profile list item', () => {
    const { asFragment } = render(
      <ProfileListItem
        selected={false}
        onChange={() => null}
        image="http://test.io/pic_001.jpeg"
        name="Mark Zuckerberg"
        company="Facebook"
      />,
    )

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render selected profile list item', () => {
    const { asFragment } = render(
      <ProfileListItem
        selected
        onChange={() => null}
        image="http://test.io/pic_001.jpeg"
        name="Mark Zuckerberg"
        company="Facebook"
      />,
    )

    expect(asFragment()).toMatchSnapshot()
  })
})
