import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { ProfileList } from '../ProfileList'

const data = [
  {
    name: 'Mark Zuckerberg',
    image: 'https://www.test.io/pic_001.jpeg',
    company: 'Facebook',
  },
  {
    name: 'Bill Gates',
    image: 'https://www.test.io/pic_002.jpeg',
    company: 'Microsoft',
  },
]

describe('ProfileList', () => {
  it('should render profile list', () => {
    const { getByText } = render(<ProfileList data={data} />)
    getByText(/Mark Zuckerberg/i)
    getByText(/MZ/i)
    getByText(/Facebook/i)
    getByText(/Bill Gates/i)
    getByText(/BG/i)
    getByText(/Microsoft/i)
  })

  it('should toggle profile list selections', () => {
    const { getByText, getAllByRole } = render(<ProfileList data={data} />)
    const checkboxes = getAllByRole('checkbox')
    expect(checkboxes.length).toBe(2)

    fireEvent.click(getByText(/select all/i))
    expect(checkboxes.every(checkbox => checkbox.checked)).toBeTruthy()

    fireEvent.click(getByText(/select none/i))
    expect(checkboxes.every(checkbox => !checkbox.checked)).toBeTruthy()
  })

  it('should update toggle button while selecting profile', () => {
    const { getByText, getAllByRole } = render(<ProfileList data={data} />)
    getByText(/select all/i)
    const [checkbox] = getAllByRole('checkbox')
    fireEvent.click(checkbox)
    expect(checkbox.checked).toBeTruthy()
    getByText(/select none/i)
    fireEvent.click(checkbox)
    expect(checkbox.checked).toBeFalsy()
    getByText(/select all/i)
  })
})
