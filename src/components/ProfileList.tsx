import React from 'react'
import styled from 'styled-components'
import { ProfileListItem } from './ProfileListItem'
import { Button } from './Button'

const List = styled.ul`
  margin: 0;
  padding: 0;
`

type Props = {
  data: {
    image: string
    name: string
    company: string
  }[]
}

export function ProfileList({ data }: Props): JSX.Element {
  const [selections, setSelections] = React.useState(data.map(() => false))
  const hasSomeSelection = selections.some(selection => selection)

  function handleToggleSelections() {
    setSelections(selections.map(() => !hasSomeSelection))
  }

  function makeHandleSelection(id: number) {
    return function handleSelection(value: boolean) {
      setSelections(
        selections.map((selected, i) => {
          if (i === id) {
            return value
          }
          return selected
        }),
      )
    }
  }

  return (
    <div>
      <Button onClick={handleToggleSelections}>
        {hasSomeSelection ? 'SELECT NONE' : 'SELECT ALL'}
      </Button>
      <List>
        {data.map((profile, i) => (
          <ProfileListItem
            key={i}
            selected={selections[i]}
            onChange={makeHandleSelection(i)}
            image={profile.image}
            name={profile.name}
            company={profile.company}
          />
        ))}
      </List>
    </div>
  )
}
