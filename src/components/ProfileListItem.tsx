import React from 'react'
import styled, { css } from 'styled-components'
import { CheckBox } from './CheckBox'

const ListItem = styled.li<{ selected: boolean }>`
  display: flex;
  padding: 24px 24px 40px 24px;
  margin: 32px 0;
  background-color: #fefefc;
  border-radius: 8px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.2);

  &:hover {
    background-color: #f6f6f4;
  }

  ${props =>
    props.selected &&
    css`
      && {
        background-color: #dfe6e4;
      }
    `}
`

const Image = styled.div<{ src: string }>`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  width: 128px;
  height: 128px;
  margin: 0 24px;
  padding: 4px;
  background-image: ${props => `url("${props.src}")`};
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 8px;
`

const Badge = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  font-weight: 700;
  font-size: 14px;
  line-height: 1;
  color: #fff;
  background-color: #4f5b5b;
  border-radius: 100%;
  user-select: none;
`

const Details = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

const NameText = styled.div`
  font-weight: 700;
  font-size: 28px;
  color: #000;
`

const CompanyLabel = styled.div`
  font-weight: 700;
  font-size: 16px;
  line-height: 1.5;
  color: #8c929d;
`

const CompanyText = styled.div`
  font-weight: 400;
  font-size: 24px;
  color: #736b6b;
`

type Props = {
  selected: boolean
  onChange: (selected: boolean) => void
  image: string
  name: string
  company: string
}

export function ProfileListItem(props: Props): JSX.Element {
  const { selected, onChange, image, name, company } = props
  const badgeText = name
    .toUpperCase()
    .split(' ')
    .map(part => part[0])
    .join('')

  return (
    <ListItem selected={selected}>
      <CheckBox checked={selected} onChange={onChange} />
      <Image src={image}>
        <Badge>{badgeText}</Badge>
      </Image>
      <Details>
        <NameText>{name}</NameText>
        <div>
          <CompanyLabel>COMPANY</CompanyLabel>
          <CompanyText>{company}</CompanyText>
        </div>
      </Details>
    </ListItem>
  )
}
