import React from 'react'
import styled from 'styled-components'

const Container = styled.span`
  position: relative;
`

const Input = styled.input.attrs({
  type: 'checkbox',
})`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  opacity: 0;
  z-index: 1;
  cursor: pointer;
`

function Icon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="#4F5B5B"
      {...props}
    >
      <path d="M5 2c-1.654 0-3 1.346-3 3v14c0 1.654 1.346 3 3 3h14c1.654 0 3-1.346 3-3v-14c0-1.654-1.346-3-3-3h-14zm19 3v14c0 2.761-2.238 5-5 5h-14c-2.762 0-5-2.239-5-5v-14c0-2.761 2.238-5 5-5h14c2.762 0 5 2.239 5 5z" />
    </svg>
  )
}

function CheckedIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="#4F5B5B"
      {...props}
    >
      <path d="M19 0h-14c-2.762 0-5 2.239-5 5v14c0 2.761 2.238 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-8.959 17l-4.5-4.319 1.395-1.435 3.08 2.937 7.021-7.183 1.422 1.409-8.418 8.591z" />
    </svg>
  )
}

type Props = {
  checked: boolean
  onChange: (checkbox: boolean) => void
}

export function CheckBox(props: Props): JSX.Element {
  const { checked, onChange } = props

  return (
    <Container>
      <Input checked={checked} onChange={() => onChange(!checked)} />
      {checked ? <CheckedIcon /> : <Icon />}
    </Container>
  )
}
