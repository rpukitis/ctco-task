import React from 'react'
import styled from 'styled-components'
import { GlobalStyles } from './components/GlobalStyles'
import { ProfileList } from './components/ProfileList'
import img1 from './images/mark-zuckerberg.jpeg'
import img2 from './images/bill-gates.jpeg'
import img3 from './images/elon-musk.jpeg'
import img4 from './images/linus-torvalds.jpeg'

const profiles = [
  {
    name: 'Mark Zuckerberg',
    image: img1,
    company: 'Facebook',
  },
  {
    name: 'Bill Gates',
    image: img2,
    company: 'Microsoft',
  },
  {
    name: 'Elon Musk',
    image: img3,
    company: 'PayPal',
  },
  {
    name: 'Linus Torvalds',
    image: img4,
    company: 'Linux Foundation',
  },
]

const Container = styled.div`
  max-width: 960px;
  margin: 32px auto;
`

export default function App(): JSX.Element {
  return (
    <Container>
      <ProfileList data={profiles} />
      <GlobalStyles />
    </Container>
  )
}
